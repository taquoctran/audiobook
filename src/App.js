import React, {useState} from "react";
import './App.css';
import DragMove from "./components/DragMove";

function App() {
    const [itemBlue, setItemBlue] = useState({x: 0, y: 0});
    const [itemRed, setItemRed] = useState({x: 0, y: 0});
    const handleDragMoveBlue = (e) => {
        setItemBlue({
            x: itemBlue.x + e.movementX,
            y: itemBlue.y + e.movementY
        });
    };
    const handleDragMoveRed = (e) => {
        setItemRed({
            x: itemRed.x + e.movementX,
            y: itemRed.y + e.movementY
        });
    };

    const setStyleMove = (item) => {
        return {transform: `translateX(${item.x}px) translateY(${item.y}px)`}
    }
  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
            <p>Hold and move item</p>
            <p>check deploy</p>
            <DragMove onDragMove={handleDragMoveBlue}>
                <div className="item blue" style={setStyleMove(itemBlue)}></div>
            </DragMove>
            <DragMove onDragMove={handleDragMoveRed}>
                <div className="item red" style={setStyleMove(itemRed)}></div>
            </DragMove>
        </div>
      </header>
    </div>
  );
}

export default App;
